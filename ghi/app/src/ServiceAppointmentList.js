function ServiceAppointmentList(props) {
    return (
      <div className="container">
        <h2 className="display-5 fw-bold">Make a Service Appointment</h2>
        <div className="row">
          {props.serviceappointment.map(serviceappointment => {
            console.log("FISH)", serviceappointment)
            return (
              <div key={serviceappointment.id} className="col">
                  
                <div className="card mb-3 shadow">
                  <div className="card-body">
                    <h5 className="card-title">{serviceappointment.owner_name}</h5>
                    

                    <h6 className="card-subtitle mb-2 text-muted">
                      {serviceappointment.service_issue}
                    </h6>
                    <p className="card-text">
                      {serviceappointment.bin}
                    </p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  
  export default ServiceAppointmentList
  