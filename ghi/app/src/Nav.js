import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers">
                  List Manufacturers
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturers/new">
                  Create Manufacturers
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/models">
                  List Vehicle Models
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/create">
                Create Vehicle Models
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">
                List Automobiles
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">
                Create Automobiles
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/service">
                Service
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/service/TechnicianForm"> 
                Technician Form
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/service/AppointmentList"> 
                Service Appointment List
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/service/AppointmentForm"> 
                Service Appointment Form
                </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/service/HistoryList"> 
                Service History List
                </NavLink>
            </li>

            <li>
              <NavLink className="nav-link" to="/salespersons/create">
                  Add Sales Person
                </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/salespersons">
                  List Sales People
                </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/customers">
                  Add Potential Customer
                </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/saleshistory">
                  List Sales History
                </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/sales">
                  Create a Sale Record
                </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
