import React from "react";


function AutomobileTable(props) {
    return (
        <tr key={props.automobile.id}>
            <td>{ props.automobile.color }</td>
            <td>{ props.automobile.year }</td>
            <td>{ props.automobile.vin }</td>
            <td>{ props.automobile.picture_url }</td>
        </tr>
    )
}
  
class AutomobileList extends React.Component {
    constructor(props) {
        super(props);
  
        this.state = {
            automobiles: [],
        };
  
    }
  
    async componentDidMount() {
        const url = "http://localhost:8100/api/automobiles/";
            const response = await fetch(url);
            const data = await response.json();

            if (response.ok) {
                console.log(data)
                this.setState({ automobiles: data.autos });
            }
       
    }
  
    render() {
        return (
            <>
            <div className="px-4 py-5 my-5">
                <h1 className="display-2 fw-bold text-center">automobiles</h1>
                <table className="table .table-bordered table-striped table-success text-left">
                    <thead>
                        <tr>
                        <th >Name</th>
                        <th>Color</th>
                        <th> Vin Number</th>
                        <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.automobiles.map((automobile) => {
                            return (
                                <AutomobileTable
                                automobile={automobile}
                                key={automobile.id}
                               
                                />
                            );
                        })}
                    </tbody>
                </table>
            </div>
            </>
        );
    }
}
  
  export default AutomobileList;

