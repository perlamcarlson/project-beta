import { NavLink } from 'react-router-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
///// INVENTORY ////////
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModelsList';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointmentList';

import ManufacturerForm from './ManufacturersForm' ;
////// Sales //////////
import SalesPersonForm from './SalesPersonForm' ;
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SalesPersonHistory from './SalesPersonHistory';
import SalesRecordList from './SalesRecordList' ;

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
              <Route path="" element={<ManufacturersList />} />
              <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
              <Route path="" element={<ModelsList />} />
          </Route>
          <Route path="automobiles">
              <Route path="" element={<AutomobileList />} />
              <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="service">
              <Route path="TechnicianForm" element={<TechnicianForm />} />
          </Route>
          <Route path="service">
              <Route path="AppointmentList" element={<ServiceAppointmentList serviceappointment={props.serviceappointment}/>} />
          </Route>
          <Route path="service">
              <Route path="AppointmentForm" element={<ServiceAppointmentForm />} />
          </Route>
          
            <Route path="salespersons/create" element={<SalesPersonForm />} />

            <Route path="salespersons" element={<SalesPersonHistory />} />
          
            <Route path="customers" element={<CustomerForm/>}/>

            <Route path="saleshistory" element={<SalesRecordList/>}/>

            <Route path="sales" element={<SalesRecordForm/>}/>
            
          </Routes>
      </div>
    </BrowserRouter>
  );
 }
  
 




export default App;
