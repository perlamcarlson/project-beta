import React from 'react';


class ServiceAppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin_number: "",
      owner_name: "",
      date_of_service: "",
      time_of_service: "",
      service_issue: "",
      technician: []
    };
    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeVinNumber = this.handleChangeVinNumber.bind(this);
    this.handleChangeOwnerName = this.handleChangeOwnerName.bind(this);
    this.handleChangeDateOfService = this.handleChangeDateOfService.bind(this);
    this.handleServiceIssue = this.handleServiceIssue.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this)
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/appointments';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ value: data.value });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.value;
  

    const locationUrl = 'http://localhost:8080/api/appointments';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newService = await response.json();
      console.log(newService);
      this.setState({
        vin_number: "",
        owner_name: "",
        date_of_service: "",
        time_of_service: "",
        service_issue: "",
        technician: []
      });
    }
  }

  handleChangeVinNumber(event) {
    const value = event.target.value;
    this.setState({ vin_number: value });
  }

  handleChangeOwnerName(event) {
    const value = event.target.value;
    this.setState({ owner_name: value });
  }

  handleChangeDateOfService(event) {
    const value = event.target.value;
    this.setState({ date_of_service: value });
  }

  handleChangeTimeOfService(event) {
    const value = event.target.value;
    this.setState({ time_of_service: value });
  }

  handleChangeServiceIssue(event) {
    const value = event.target.value;
    this.setState({ service_issue: value });
  }

  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({ technician: value });


  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Make your service appointment with us</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVinNumber} value={this.state.vin_number} placeholder="vin_number" required type="text" name="vin_number" id="vin_number" className="form-control" />
                <label htmlFor="vin_number">Vin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeOwnerName} value={this.state.owner_name} placeholder="Owner_name" required type="text" name="owner_name" id="owner_name" className="form-control" />
                <label htmlFor="owner_name">Owner Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeDateOfService} value={this.state.date_of_service} placeholder="Date_of_service" required type="text" name="date_of_service" id="date_of_service" className="form-control" />
                <label htmlFor="date_of_service">Date of Service</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangePicture} value={this.state.picture_url} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeBin} value={this.state.bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.href}>{bin.closet_name.charAt(0).toUpperCase() + bin.closet_name.slice(1)} Bin:{bin.bin_number} Size:{bin.bin_size}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceAppointmentForm;
