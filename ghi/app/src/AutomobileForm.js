import React from 'react';

class AutomobileForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        color: "",
        year: "",
        vin: "",
        models: [],
      };

      this.handleChangeColor = this.handleChangeColor.bind(this);
      this.handleChangeYear = this.handleChangeYear.bind(this);
      this.handleChangeVin = this.handleChangeVin.bind(this);
      this.handleChangeModel = this.handleChangeModel.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    async componentDidMount() {
      const url = 'http://localhost:8100/api/models/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        this.setState({ models: data.models });
        
      }
    }
  
    async handleSubmit(event) {
      event.preventDefault();
      const data = {...this.state};
      delete data.models
      //it's getting strings for everything but expects year and vin 
      // to be numbers 
      console.log("THIS IS AUTOFORM DATA", data)

  
      const automobileURL = 'http://localhost:8100/api/automobiles/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const autoResponse = await fetch(automobileURL, fetchConfig);
      if (autoResponse.ok) {
        const newAutomobile = await autoResponse.json();
        console.log(newAutomobile);
        this.setState({
          vin: "",
          color: "",
          year: "",
          models: [],
        });
      }
    }
  
    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({ model_id: value });
      }
    handleChangeColor(event) {
      const value = event.target.value;
      this.setState({ color: value });
    }
    handleChangeYear(event) {
      const value = event.target.value;
      this.setState({ year: value });
    }
    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
      }
   
  

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 class= 'text-center'>Add Automobile</h1>
            <form onSubmit={this.handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
                <input onChange={this.handleChangeModel} 
                value={this.state.name} 
                placeholder="model" 
                required type="text" 
                name="model" 
                id="model" 
                className="form-control" />
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeYear} 
                value={this.state.name} 
                placeholder="Year" 
                required type="text" 
                name="year" 
                id="year" 
                className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
            <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} 
                value={this.state.name} 
                placeholder="Color" 
                required type="text" 
                name="color" 
                id="color" 
                className="form-control" />
                <label htmlFor="color">color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVin} 
                value={this.state.name} 
                placeholder="VIN" 
                required type="text" 
                name="vin" 
                id="vin" 
                className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              < div class= "d-flex justify-content-center">
                <button className="btn btn-success btn-default " type="submit" data-toggle="button">Submit</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;