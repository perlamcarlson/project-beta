import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
console.log("FROG1");
async function LoadDealership() {
  console.log("FROG");
  let serviceappointmentData;
  const serviceappointmentResponse = await fetch('http://localhost:8080/api/appointments/');
  

  if (serviceappointmentResponse.ok) {
    serviceappointmentData = await serviceappointmentResponse.json();
    console.log('serviceappointment data: ', serviceappointmentData)
  } else {
    console.error(serviceappointmentResponse);
  }
  

  root.render(
    <React.StrictMode>
      <App serviceappointment={serviceappointmentData}  />
    </React.StrictMode>
  );
}
LoadDealership();
