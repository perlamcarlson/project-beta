# CarCar

Team:

* Starr Solis - Sales
* Perla Carlson - Services

## Design
The Design consists of three microservices all operating within their bounded contacts with an integration point between the inventory api to the sales api and service api through each services poller. All of these microserves are used to build the front end. When a user opens the site they will be able to use the interactive nav bar running across the top of the page to check out different components. 

If we had more time:
We would change the navbar to have a dropdown component to declutter.
We would rename some of the Nav tags for better understanding.
We would implement a site wide color scheme and implement some cohesive styling choices between pages, for example, using cards to display some information and charts to display others between on the look we want to achieve.
We would build out the functionality of all pages currently listed and add some additional functionality such as the ability to delete and edit data from the frontend.
We would also reorganize our code to be more realable, such as using the index.js file to reduce the need for repeated code and grouping different components into folders, example, all the sales pages would be in a file names sales within the ghi/app/scr. 


## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

The Sales API is a microserve that consists in its own bounded content to provide the user with an interface that allows the tracking of the sales employees and their indvidual sales, customer information, and previous sale records. To do this I thought it would be best to build some models and views to interact with the database and then build out the frontend with 5 unique pages handling the creation and listing of various components.  

The Backend: 
The backend consists of 4 main models, AutomobileVo, Salesperson, Customer and SalesHistory and then views to handle the 5 main http requests for each sales person, sale and customer. 

Integration Point: The integration point between the sales api and the inventory api is handled by the poller.py and AutomobileVO class.

Frontend: 
The user interface was built using React and has the following pages which you can access from the navigation bar, Add Sales Person, List Sales Person, Add Potential Customer, List Sales History, and Create Sales record. 

Challenges:
Throughout this project I achieved a greater understanding of how the frontend and backend work together, however in the future I would take more time to plan out my approach to the project. For example, what do I want the user to experience, what is that page called, how does it look? I built this project backend to front, making decisions on naming, style and sometimes functionality as I went and ran into errors. I feel like if I would have spent more time in the beginning imagining front-end to back-end while also jotting down some rules for myself that I would like to follow when it comes to naming variables I could have saved myself a lot of time chasing errors. 

