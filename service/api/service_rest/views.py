from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Service, Technician
from common.json import ModelEncoder
import json

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
      "name", "employee_number",
    ]

class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin_number",
        "owner_name",
        "date_of_service",
        "time_of_service",
        "service_issue",
        
    ]
    encoders = {
        "Technician": TechnicianEncoder(),
        
    }


@require_http_methods(["GET", "POST"])
def technician_list(request):
    if request.method == "GET":
       technicians = Technician.objects.all()
       return JsonResponse(
           technicians,
           encoder=TechnicianEncoder,
           safe=False
       )
    else: #POST
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def service_appointment_list(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
           services,
           encoder=ServiceEncoder,
           safe=False
       )
    else: #POST
        content = json.loads(request.body)

        services = Service.objects.create(**content)
        return JsonResponse(
            services,
            encoder=ServiceEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def service_history_list(request):
    if request.method == "GET":
       service = Service.objects.all()
       return JsonResponse(
           service,
           encoder=ServiceEncoder,
           safe=False
       )
    else: #POST
        content = json.loads(request.body)

        service = Service.objects.create(**content)
        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False
        )    




#         service = Service.objects.create(**content)
#         return JsonResponse(
#           service,
#           encoder=ServiceEncoder,
#           safe=False
#         )


# @require_http_methods(["GET", "POST"])
# def api_service_appointment_list(request):
#     if request.method == "GET":
#       services = Service.objects.all()
#       return JsonResponse(
#         services,
#         encoder=ServiceEncoder,
#         safe=False
#       )
#     else: #POST
#         content = json.loads(request.body)


#         service = Service.objects.create(**content)
#         return JsonResponse(
#           service,
#           encoder=ServiceEncoder,
#           safe=False
#         )

# @require_http_methods(["DELETE"])
# def api_delete_hat(request, pk):
#     try:
#         hat = Hat.objects.get(id=pk)
#         hat.delete()
#         return JsonResponse(
#           hat,
#           encoder=HatEncoder,
#           safe=False,
#         )
#     except Hat.DoesNotExist:
#         return JsonResponse({"message": "Does not exist"})