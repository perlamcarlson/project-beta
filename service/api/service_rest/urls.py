from django.urls import path
from .views import service_appointment_list, technician_list


urlpatterns = [
    path('technician/',technician_list, name="technician_list"),
    path('appointments/', service_appointment_list, name="service_appointment_list"),
    
     
    
    ]