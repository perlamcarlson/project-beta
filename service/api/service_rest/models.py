from tkinter import CASCADE
from django.db import models
from django.forms import CharField


class Technician(models.Model):
    name = models.CharField(max_length=150)
    employee_number = models.BigIntegerField()

    def __str__(self):
        return self.title




class Service(models.Model):
    vin_number = models.TextField()
    owner_name = models.CharField(max_length=150)
    date_of_service = models.DateField()
    time_of_service = models.TimeField()
    service_completed = models.BooleanField(default=False)
    service_issue =  models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="service",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title








# Create your models here.
